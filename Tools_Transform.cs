﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Biblio
{
    public static class Tools_Transform
    {
        /*
         * Fonction TransformByteToString
         * Cette fonction sert a transformer un tableau de byte de taille inconnu en chaine de charactére string
         * la transformation se fait sur l'intégraliter de la chaine data
         * Encoding.ASCII.GetString(chaine_recu, 0, bytesRec) ou chaine_recu
         */
        public static string transformbytetostring(byte[] chaine_recu)
        {
            string data = "";
                int bytesRec = chaine_recu.Length;
                data += Encoding.ASCII.GetString(chaine_recu, 0, bytesRec);
           
            return data;
        }

        /*
         * Fonction TransformStringToByte
         * Cette fonction sert a transformer un string de taille inconnu en tableau de Byte
         * On sait que l'échange part d'un encodage ASCII
         * Via Encoding.ASCII.GetBytes(swap) on récupére le tableau de byte qui représente notre chaine de string
         */
        public static byte[] TransformStringToByte(string swap)
        {
            byte[] data = new byte[1024];
            data = Encoding.ASCII.GetBytes(swap);
            return data;
        }

        /*
         * Fonction TransformCooToByte
         * Cette fonction sert a transformer deux coordonée X et Y en chaine de charactére représentant l'envoie d'une futur graine de café
         * On sait que l'échange part d'un encodage ASCII
         * Via Encoding.ASCII.GetBytes(swap) on récupére le tableau de byte qui représente notre chaine de string
         */

        public static byte[] TransformCooToByte(int x,int y)
        {
            byte[] data = new byte[1024];

            data = Encoding.ASCII.GetBytes("A:" + x.ToString() + y.ToString());

            return data;
        }

        /*
         * Fonction TransformByteToCoo
         * Cette fonction sert a transformer un tableau de byte de taille inconnu en tableau d'entier
         * la transformation se fait sur l'intégraliter de la chaine data
         * Encoding.ASCII.GetString(chaine_recu, 0, bytesRec) ou chaine_recu
         * Ensuite on parse les deux chiffre et retourne un tableau de coordonee x et Y
         */
        public static int[] TransformByteToCoo(byte[] coo_recu_du_serveur)
        {
            string data = "";
            int[] coo = new int[2]; int swap;
            int bytesRec = coo_recu_du_serveur.Length;
            data += Encoding.ASCII.GetString(coo_recu_du_serveur, 0, bytesRec);
            string[] temporaire = data.Split(':');
            Int32.TryParse(temporaire[1], out swap);
            coo[0] = (int)swap / 10;
            coo[1] = (int)swap - (coo[0] * 10);
            return coo;
        }
    }
}
