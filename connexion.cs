﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net; // pour les outils réseau
using System.Net.Sockets; // pour les sockets

namespace Biblio
{
    class connexion
    {
        /*
         * La classe connexion sert a interagir avec un serveur via un systéme de Socket de façon synchroniser
         * REMARQUE PERSO: le faire de façon asynchronee aurait pus permettre d'initier un chat ou autre voir de creer un jeux ,le serveur alors n'enverrai que les élément changeant des autre joueur
         * la classe Socket est uttiliser en deux temps, lors de l'initialisation avec son type de connection InterNetwork qui revient a dire qu'on uttilise de l'IPV4
         * SocketType.Stream qui revient préciser que la connection sera maintenue active tout aux long de l'échange
         * et protocol type qui me parait plutot explicit ici TCPS
         */
        byte[] IP = new byte[4]; //tableau de byte qui servira a stocker l'addresse du serveur
        IPAddress local; //variable qui contiendra l'address ip du serveur sous un format lisible par le socket
        int port = 25565;//port de connection entre le client et le serveur
        Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); //initialisation du type de connection
        int statut = 0; //statut = 0 veux dire que la classe est a son état initiale, =1 qu'il est connecter, =-1 est qu'on est en erreur

        /*
         * Le constructeur a pour fonction de de creer l'addresse ip via la methode ci dessous inttobyte qui passe quatre entier dans un tableau de byte
         * et de passer le garder le port dans la class
         */
        public connexion(int premier, int deuxieme, int troisieme, int quatrieme, int portip)
        {
            this.port = portip;
            IP = inttobyte(premier, deuxieme,troisieme,quatrieme);
            local = new IPAddress(IP);
        }

        /*
         * La méthode connect qui ne recois aucun paramétre mais qui retourne un premier tableau de byte
         * le tableau de variable byte retour servira a retourner les valeur recu ou d'erreur
         * trois cas sont tréter dans la méthode:
         * 1-tout fonctionne correctement et la fonction retourne ce qu'elle a reçu du serveur
         * 2-la connection a déja été instancier alors on retourne un tableau vide
         * 3- une erreur est déclarer et la fonction passe en erreur
         * 
         * Mais une situation ne peux étre traiter dans cette version, et si le serveur distant ne peux étre joint et donc une attente infini finis par arriver ?
         * Une solution serait une connection asynchrone avec un timer pour retourner une erreur
         * un test en local a été effectuer et montre que le socket restera a attendre ne boucle
         */
        public byte[] Connect()
        {
            byte[] retour;
            try
            {
                if (statut==0)
                {
                    
                    listener.Connect(local, port);
                    retour = receive();
                    statut = 1;
                }
                else
                {
                    retour =new byte[0];
                }
                return retour;
            }
            catch(Exception ee)
            {
                statut = -1;
                throw ee;
            }
        }

        /*
         * La méthode disconnect permettra de déconnecter le socket dans le cas ou précédement il a été instancier et remettra par conséquent le statut a 0
         */
        public void disconect()
        {
            if(statut ==1)
            {
                listener.Shutdown(SocketShutdown.Both);
                listener.Close();
                statut = 0;
            }
        }

        /*
         * Essai de l'envoie du paramétre tosend aux serveur distant
         * Prise en compte d'une posibilité d'erreur
         */
        public void send(byte[] tosend)
        {
            try
            {
                listener.Send(tosend);
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }

        /*
         * la méthode test la réception d'un tableau de byte en provenance du serveur
         * Prise en compte d'un caas ou le socket envoie une exception
         */
        public byte[] receive()
        {
            try
            {
                byte[] retour = new byte[1024];
                listener.Receive(retour);
                return retour;
            }
            catch(Exception ee)
            {
                throw ee;
            }
            
        }

        /*
         * Fonction qui transforme quatre entier en retourne un tableau de byte via des caste
         */
        public byte[] inttobyte(int premier, int deuxieme,int troisieme, int quatrieme)
        {
            byte[] swap = new byte[4];
            swap[0] = (byte)premier;
            swap[1] = (byte)deuxieme;
            swap[2] = (byte)troisieme;
            swap[3] = (byte)quatrieme;
            return swap;
        }
    }
}
