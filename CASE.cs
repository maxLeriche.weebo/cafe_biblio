﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Biblio
{
    public class CASE
    {
        //Booléen si oui ou non la case a un frontiére
        bool nORD = false;
        bool eST = false;
        bool sUD = false;
        bool oUEST = false;
        bool eAU = false;
        bool fORET = false;

        public bool FORET { get => fORET; }
        public bool EAU { get => eAU; }
        public bool OUEST { get => oUEST; }
        public bool SUD { get => sUD; }
        public bool EST { get => eST; }
        public bool NORD { get => nORD; }

        public CASE(int indicateur)
        {
            if ((indicateur-64)>0)
            {
                this.eAU = true;
                indicateur = indicateur - 64;
            }
            else if ((indicateur-32>0))
            {
                this.fORET = true;
                indicateur = indicateur - 32;
            }
            if (indicateur>=8)
            {
                this.eST = true;
                indicateur = indicateur - 8;
            }
            if(indicateur>=4)
            {
                this.sUD = true;
                indicateur = indicateur - 4;
            }
            if(indicateur>=2)
            {
                this.oUEST = true;
                indicateur = indicateur - 2;
            }
            if (indicateur>=1)
            {
                this.nORD = true;
                indicateur = indicateur - 1;
                
            }
            

        }

        public void NORD_true()
        {
            this.nORD = true;
        }
        public void NORD_false()
        {
            this.nORD = false;
        }

        public void EST_true()
        {
            this.eST = true;
        }
        public void EST_false()
        {
            this.eST = false;
        }

        public void SUD_true()
        {
            this.sUD = true;
        }
        public void SUD_false()
        {
            this.sUD = false;
        }

        public void OUEST_true()
        {
            this.oUEST = true;
        }
        public void OUEST_false()
        {
            this.oUEST = false;
        }
    }
}
