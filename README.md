Biblioth�que de Classe de la Bataille du caf�

Voici les r�gle:
- R1. La premi�re pose est libre.
- R2. Il est interdit de placer une graine sur une unit� de type Mer ou For�t.
- R3. Un joueur doit poser une graine sur la m�me ligne ou la m�me colonne que la derni�re graine
plac�e.
- R4. Un planteur ne peut pas planter une graine sur la m�me parcelle que la derni�re graine pos�e.
- R5. En cas de pose invalide, le planteur perd sa graine et son tour.
- R6. Si aucune des cases disponibles ne satisfait les r�gles R3 et R4, alors la partie est termin�e.
- R7. D�s qu'un planteur a utilis� ses 28 graines, la partie est �galement termin�e.