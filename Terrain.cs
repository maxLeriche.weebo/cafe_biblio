﻿using System;
using System.Text;

namespace Biblio
{
    public class Terrain
    {
        CASE[][] terrain = new CASE[10][];
        char[][] placer = new char[10][];
        int tour = 0;
        int a_placer = 28;

        public Terrain()
        {
            init_terrain();

            
        }

        //Fonction Getter qui permet de retrourner le tableau de CASE
        public CASE[][] TERRAIN { get => terrain;  }
        //Fonction Getter qui permet de retourner le tableau des Graine placer
        public char[][] GrainePlacer { get => placer;  }

        //Permet d'init ou de reset le tableau en cas de probléme
        public void init_terrain()
        {
            for (int index = 0; index < 10; index++)
            {
                this.terrain[index] = new CASE[10];
                this.placer[index] = new char[10];
            }
        }

        //Fonction qui permet de definir le tableau Terrain a partir de la chaine décoder en string
        public void set_terrain_from_string(string recu)
        {
            string[] découper = recu.Split('|');
            string[][] swap = new string[découper.Length - 1][];
            for (int index = 0; index < découper.Length - 1; index++)
            {
                swap[index] = découper[index].Split(':');
            }
            for (int index = 0; index < 10; index++)
            {
                for (int indey = 0; indey < 10; indey++)
                {
                    int valeurCase=0;
                    Int32.TryParse(swap[index][indey], out valeurCase);
                    this.terrain[index][indey] =new CASE(valeurCase);
                }
            }
        }
        
        //Fonction qui permet de placer un pion sur le plateau placer
        public void Placement_Pion(bool ServeurQuiJoue,int CooX,int CooY)
        {
            char couleur;
            if(ServeurQuiJoue)
            {
                couleur = 'S';
            }
            else if(!ServeurQuiJoue)
            {
                couleur = 'c';
            }
            else
            {
                Exception test = new Exception("Erreur True=Serveur false=Client");
                throw test;
            }
            placer[CooX][CooY] = couleur;
        }

    }
}
